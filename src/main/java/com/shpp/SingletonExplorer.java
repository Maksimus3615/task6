package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingletonExplorer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SingletonExplorer.class);

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            String mess = Singleton.getInstance().toString(); // it's the same object
            LOGGER.info(mess);
        }
    }
}

class Singleton {

    private static final Singleton instance = new Singleton(); //it is good for multithreading...

    private Singleton() {
    }

    public static Singleton getInstance() {
        return instance;
    }
}


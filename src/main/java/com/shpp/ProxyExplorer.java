package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProxyExplorer {
    public static void main(String[] args) {
        SomeInterface object = new ProxyClass();
        object.doSomething();
    }
}

interface SomeInterface {
    void doSomething();
}

class ProxyClass implements SomeInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProxyClass.class);
    SomeInterface object = new SomeClass();

    @Override
    public void doSomething() {
        LOGGER.info("This is proxy object...");
        object.doSomething();
    }
}

class SomeClass implements SomeInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(SomeClass.class);

    @Override
    public void doSomething() {
        LOGGER.info("This is real object...");
    }
}
package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdapterExplorer {
    public static void main(String[] args) {
        DesiredInterface object = new Adapter();
        object.doSomething();
    }
}

class Adapter extends ClassContainsDesiredMethod implements DesiredInterface {

    @Override
    public void doSomething() {
        this.desiredMethod();
    }
}

interface DesiredInterface {
    void doSomething();
}

class ClassContainsDesiredMethod {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassContainsDesiredMethod.class);

    void desiredMethod() {
        LOGGER.info("Hello! This is desired method...");
    }
}
package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StrategyExplorer {
    public static void main(String[] args) {
        Context context = new Context();

        context.setStrategy(new FirstStrategyWay());
        context.executeStrategy();

        context.setStrategy(new SecondStrategyWay());
        context.executeStrategy();

        context.setStrategy(new ThirdStrategyWay());
        context.executeStrategy();
    }
}

class Context {
    Strategy strategy;

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void executeStrategy() {
        strategy.doThisWay();
    }
}

interface Strategy {
    void doThisWay();
}

class FirstStrategyWay implements Strategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstStrategyWay.class);

    @Override
    public void doThisWay() {
        LOGGER.info("This is first way of strategy...");
    }
}

class SecondStrategyWay implements Strategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecondStrategyWay.class);

    @Override
    public void doThisWay() {
        LOGGER.info("This is second way of strategy...");
    }
}

class ThirdStrategyWay implements Strategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdStrategyWay.class);

    @Override
    public void doThisWay() {
        LOGGER.info("This is third way of strategy...");
    }
}
package com.shpp.real;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientAsProxyExplorer {
    public static void main(String[] args) {
        Server object = new ProxyServer();
        object.showWebSite();
    }
}

interface Server {
    void showWebSite();
}

class ProxyServer implements Server {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProxyServer.class);
    Server object = new RealServer();

    @Override
    public void showWebSite() {
        LOGGER.info("This is proxy site...");
        object.showWebSite();
    }
}

class RealServer implements Server {
    private static final Logger LOGGER = LoggerFactory.getLogger(RealServer.class);

    @Override
    public void showWebSite() {
        LOGGER.info("This is real site...");
    }
}
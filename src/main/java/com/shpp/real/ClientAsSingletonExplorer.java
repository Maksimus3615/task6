package com.shpp.real;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientAsSingletonExplorer {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAsSingletonExplorer.class);

    public static void main(String[] args) {

        Connection connection1 = Connection.getInstance();
        String mess = connection1.toString();
        LOGGER.info(mess);

        Connection connection2 = Connection.getInstance();
        mess = connection2.toString();
        LOGGER.info(mess);

        Connection connection3 = Connection.getInstance();
        mess = connection3.toString();
        LOGGER.info(mess);
    }
}

class Connection {
    private static final Connection instance = new Connection();     //it is good for multithreading...

    private Connection() {
    }

    public static Connection getInstance() {
        return instance;
    }
}
//class Connection {
//    private static Connection instance;
//    private Connection() {
//    }
//    public static Connection getInstance() {
//        if (instance == null)
//            instance = new Connection();
//        return instance;
//    }
//}


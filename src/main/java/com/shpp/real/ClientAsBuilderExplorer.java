package com.shpp.real;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientAsBuilderExplorer {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientAsBuilderExplorer.class);

    public static void main(String[] args) {
        PersonBuilder personBuilder =new PersonBuilder();
        Person person = personBuilder
                .setName("Max")
                .setAge(42)
                .setProfession("hydraulic")
                .setSalary(100000)
                .setTaxNumber(123456789)
                .build();
        String mess = person.toString();
        LOGGER.info(mess);
    }
}

class Person {
    String name;
    int age;
    String profession;
    int salary;
    int taxNumber;

    @Override
    public String toString() {
        return "\n" + name + ", " +
                age + ", " +
                profession + ", " +
                salary + ", " +
                taxNumber;
    }
}


class PersonBuilder {
    Person person = new Person();

    public PersonBuilder setName(String name) {
        person.name = name;
        return this;
    }
    public PersonBuilder setAge(int age) {
        person.age = age;
        return this;
    }

    public PersonBuilder setProfession(String profession) {
        person.profession = profession;
        return this;
    }

    public PersonBuilder setSalary(int salary) {
        person.salary = salary;
        return this;
    }

    public PersonBuilder setTaxNumber(int taxNumber) {
        person.taxNumber = taxNumber;
        return this;
    }

    public Person build() {
        return person;
    }
}
package com.shpp.real;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientAsAdapterExplorer {
    public static void main(String[] args) {
        InterfaceUSB connector = new Adapter();
        connector.connectToUSB();
    }
}

class Adapter extends MicroUSB implements InterfaceUSB {

    @Override
    public void connectToUSB() {
        this.connectToMicroUSB();
    }
}

interface InterfaceUSB {

    void connectToUSB();
}

class MicroUSB {
    private static final Logger LOGGER = LoggerFactory.getLogger(MicroUSB.class);

    void connectToMicroUSB() {
        LOGGER.info("********This is microUSB connection...");
    }
}
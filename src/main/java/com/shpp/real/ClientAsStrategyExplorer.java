package com.shpp.real;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientAsStrategyExplorer {
    public static void main(String[] args) {
        Way way = new Way();

        way.setWay(new GetByPlane());
        way.executeWay();

        way.setWay(new GetByCar());
        way.executeWay();

        way.setWay(new GetByShip());
        way.executeWay();
    }
}

class Way {
    Strategy strategy;

    public void setWay(Strategy strategy) {
        this.strategy = strategy;
    }

    public void executeWay() {
        strategy.doThisWay();
    }
}

interface Strategy {
    void doThisWay();
}

class GetByPlane implements Strategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetByPlane.class);

    @Override
    public void doThisWay() {
        LOGGER.info("This is the first way - get by plane...");
    }
}

class GetByCar implements Strategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetByCar.class);

    @Override
    public void doThisWay() {
        LOGGER.info("This is the second way - get by car...");
    }
}

class GetByShip implements Strategy {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetByShip.class);

    @Override
    public void doThisWay() {
        LOGGER.info("This is the third way - get by ship...");
    }
}